import axios from "axios";
import { BaseURL } from "../config";

const axiosInstance = axios.create({
  baseURL: BaseURL.baseUrl
});

export const getRequest = (url :string) => {
  return axiosInstance({
    method: "GET",
    url
  });
};

export const postRequest = (url :string, data :object) => {
  return axiosInstance({
    method: "POST",
    url,
    data
  });
};
