import React, { Component } from "react";
import { connect } from "react-redux";
import { AppState } from "../store/store";
import Conversation from "./conversation/Conversation";
import { MainComponentExpense } from './ComponentsExpense'
import MultipleChatScreen from "./conversation/MultipleChatScreen";
import ChatScreen from "./conversation/ChatScreen";


 class MainComponent extends Component <MainComponentExpense> {
  render() {
    const { userData = [] } = this.props.conversation;
    return (
      <div>
        <Conversation />
        {userData && userData.length > 0 ? (
          <div className="chat-screen-block-all">
            <ul className="multiple-chat-block">
              {userData.map((element : any, index :number) =>
                index >= 3 ? (
                  <MultipleChatScreen dataId={element.channel_id} key={index} />
                ) : null
              )}
            </ul>
            <ul className="chat-screen-block">
              {userData.map((element :any, index :number) =>
                index <= 2 ? (
                  <ChatScreen dataId={element.channel_id} key={index} />
                ) : null
              )}
            </ul>
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    conversation: state.conversation
  };
};

export default connect(
  mapStateToProps,
  {}
)(MainComponent);
