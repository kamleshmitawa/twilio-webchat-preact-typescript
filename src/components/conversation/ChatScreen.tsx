import React, { Component } from "react";
import { connect } from "react-redux";
import conversation from "../../store/actions";
import ChatMessage from "./ChatMessage";
import { fetchChatToken } from "../../utils/utils";
import { postRequest } from "../../api/api";
import { MainComponentExpense, mainComponentProps, mainComponentState } from "../ComponentsExpense";
import { AppState } from "../../store/store";



declare const window: any;
let Twilio = window.Twilio


class ChatScreen extends Component<mainComponentProps, mainComponentState> {
  public chatClient : any;
  private channel : any;

  constructor(props : mainComponentProps) {
    super(props);
    this.state = {
      loading: true,
      isCollapse: false,
      inputMessage: ""
    };
  }

  componentDidMount = async () => {
    this.initialChatToken();
  };

  componentWillUnmount() {
    console.log("unmount");
  }

  initialChatToken = async () => {
    fetchChatToken().then(token => {

      Twilio.Chat.Client.create(token).then((client :any) => {

        if (this.chatClient) return;
        this.chatClient = client;

        client.on("channelJoined", (channel :any) => {
          console.log(channel.members, "channelchannel");

          channel.on("messageAdded", (message :any) => {
            console.log("messageAddedmessageAdded", message);
            conversation.newMessage(message);
          });

          channel.on("memberJoined", (member :any) => {
            console.log("member Joined", member.identity);
          });

          channel.on("memberLeft", (member :any) => {
            console.log("member Left", member.identity);
          });

          channel.on("memberInfoAdded", (member :any) => {
            console.log("member info updated", member.identity);
          });

          channel.on("typingStarted", (member :any) => {
            console.log("typing started", member.identity);
          });

          channel.on("typingEnded", (member :any) => {
            console.log("member Left", member.identity);
          });

          channel.invite("elmo").then(() => {
            console.log("Your friend has been invited!");
          });
        });

        client.on("channelAdded", (channel :any) => {
          console.log("Channel added channel.friendlyName:=========" + channel.friendlyName);
        });

        client.on("channelRemoved", (channel :any) => {
          console.log("channel Removed", channel.friendlyName);
        });

        client.on("channelUpdated", (channel :any) => {
          console.log("channel Updated", channel.friendlyName);
        });

        client.on("channelInvited", (channel :any) => {
          console.log("Invited to channel " + channel.friendlyName);
          channel.join();
        });

        client.on("tokenExpired", this.HandleRefereshToken);

        this.getChatRoom()
          .then((channel :any) => {
            this.setState({ loading: false });
          })
          .catch((err :any) => console.log(err, "create channel error"));
      });
    });
  };

  getChatRoom = () => {
    const { conversationData } = this.props;
    return this.chatClient
      .getChannelBySid(conversationData.channel_id)
      .then((channel :any) => {
        this.channel = channel;
        if (channel.state.status !== "joined") {
          channel.join();
        }
        return channel;
      })
      .catch((err :any) => console.log(err, "---=====createChannel error"));
  };



  HandleRefereshToken = () => {
    return fetchChatToken().then(token => {
      this.chatClient.updateToken(token);
    });
  };

  closeChatScreen = (e :any, data :any) => {
    conversation.chatScreenClose(data);
    e.stopPropagation();
  };

  sendMessageToCustomer = () => {
    const { inputMessage } = this.state;

    let data = {
      channelId: this.channel.sid,
      message: inputMessage,
      isSentByAgent: true
    };

    return postRequest("/web/agent-send-message", data)
      .then(res => {
        console.log(res, "sendMessage res");
        this.setState({ inputMessage: "" });
      })
      .catch(err => console.log(err, " sendMessage err"));
  };

  onEnterSendMessage = (e :any) => {
    if (e.key === "Enter") {
      this.sendMessageToCustomer();
    }
  };

  render() {
    const { isCollapse, inputMessage, loading } = this.state;
    const { conversationData } = this.props;
    const { messages = [] } = conversationData;
   
    return (
      <li className={
          isCollapse
            ? "chat-screen-block-item chat-screen-collapse-block"
            : "chat-screen-block-item"
        }
      >
        <div
          className="conversation-header"
          onClick={() => this.setState({ isCollapse: !this.state.isCollapse })}
        >
          <img
            src="assests/images/avatar.png"
            className="chat-user-profile-image"
            alt="profileimg"
          />
          <span className="chat-user-name">
            {conversationData && conversationData.unique_name
              ? "Anonymous1"
              : "Anonymous"}
          </span>
          <i
            onClick={e => this.closeChatScreen(e, conversationData)}
            className={"fa fa-times-circle-o chat-close-icon"}
            aria-hidden="true"
          ></i>
        </div>
        <div className="chat-block">
          {/* {loading ? "loading" : <ChatMessage messages={messages} />} */}
           <ChatMessage messages={messages} />
        </div>
        <div className="chat-message-input-block">
          <input
            onKeyDown={this.onEnterSendMessage}
            value={inputMessage}
            onChange={e => this.setState({ inputMessage: e.target.value })}
            type="text"
            placeholder="Message Input ..."
            name="input"
            className="chat-message-input"
          />

          <button onClick={this.sendMessageToCustomer}>Send</button>
        </div>
      </li>
    );
  }
}

const mapStateToProps = (state : AppState, ownProps :MainComponentExpense) => {
  return {
    // conversationList1: state.conversation.conversationList,

    conversationData: state.conversation.conversationList.find(
      val => val.channel_id === ownProps.dataId
    )
  };
};

export default connect(mapStateToProps)(ChatScreen);
