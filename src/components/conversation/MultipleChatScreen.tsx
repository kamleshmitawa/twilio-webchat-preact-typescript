import React, { Component } from "react";
import { mainComponentProps } from "../ComponentsExpense";

class MultipleChatScreen extends Component<mainComponentProps> {
  render() {
    const { data } = this.props;

    return (
      <li className="multiple-chat-item">
        <div
          className="conversation-header"
          //   onClick={() => this.setState({ isCollapse: !this.state.isCollapse })}
        >
          {data && data.unique_name ? "Anonymous1" : "Anonymous"}
        </div>
        <i
          // onClick={(e) => this.closeChatScreen(e,data)}
          className={"fa fa-times-circle-o chat-close-icon"}
          aria-hidden="true"
        ></i>
      </li>
    );
  }
}

export default MultipleChatScreen;
