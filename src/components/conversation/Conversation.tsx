import React, { Component } from "react";
import { connect } from "react-redux";
import conversation from "../../store/actions";
import { getRequest } from "../../api/api";
import { fetchSyncToken } from "../../utils/utils";
import { AppState } from "../../store/store";

interface componentState {
  [key: string]: any,
}
interface statemy {
  [key: string]: any,
}
interface itmeExpense {
  [key: string]: any,
}

declare const window: any;
let Twilio = window.Twilio

class Conversation extends Component<statemy, componentState> {
  constructor(props: any) {
    super(props);
    this.state = {
      loading: true,
      isBlockClose: false
    };
  }

  componentDidMount() {
    getRequest("/web/getChannel")
      .then(res => {
        console.log(res.data.data, 'getChannelres')
        this.setState({
          loading: false
        });
        conversation.conversationList(res.data.data);
      })
      .catch(err => console.log(err, "getChannel error"));

    this.initializeSyncClient();
  }

  handleChatScreen = (data: any) => {
    conversation.chatScreenOpen(data);
  };


  initializeSyncClient = () => {
    fetchSyncToken().then(token => {
      var syncClient = new Twilio.Sync.Client(token);

      syncClient.document('MyDocument')
        .then((document: any) => {
          console.log('Successfully opened a Document. SID: ' + document.sid);
          document.on('updated', (event: any) => {
            console.log('Received updated event: ', event);
          });
          document.on('added', (event: any) => {
            console.log('Received added event: ', event);
          });
        })
        .catch((error: object)=> {
          console.log('Unexpected error', error);
        });


      syncClient.map('users').then((map: any) => {
        map.on('itemAdded', (item: any) => {
          console.log('JSON data', item.value);
        });

        map.on('itemUpdated', (item: any) => {
          console.log('JSON data', item.value);
        });
      });

    })
      .catch(err => console.log(err, 'sync token err'))
  }

  render() {
    const { state } = this;
    const { conversationList = [] } = this.props;
    return (
      <div
        className={
          state.isBlockClose ? "conversation-block close-block" : "conversation-block"
        }
      >
        <div
          className="conversation-header"
          onClick={() => this.setState({ isBlockClose: false })}
        >
          Conversation
        </div>
        <div onClick={() => this.setState({ isBlockClose: true })}>
          <i
            className={
              state.isBlockClose
                ? "fa fa-times-circle-o close-icon close-block"
                : "fa fa-times-circle-o close-icon"
            }
            aria-hidden="true"
          ></i>
        </div>
        <div className="conversation-list-block">
          {conversationList && conversationList.length > 0 ? (
            conversationList.map((item: itmeExpense, index: any) => (
              <div
                className="conversation-list-item"
                key={index}
                onClick={() => this.handleChatScreen(item)}
              >
                <img
                  src="assests/images/avatar.png"
                  className="profile-image"
                  alt="profileimg"
                />
                <div className="user-name">
                  {index}
                  {/* {item && item.unique_name ? "Anonymous1" : "Anonymous"} */}
                </div>
                <div className="conversation-list-message">
                  {item && item.messages && item.messages.length > 0
                    ? item.messages[item.messages.length - 1].body
                    : null}
                </div>
              </div>
            ))
          ) :
            conversationList && conversationList.length === 0 ?
              <div className="conversation-list-item">No Conversations</div>
              :
              (
                <div className="conversation-list-item">...loading</div>
              )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  return {
    conversationList: state.conversation.conversationList
  };
};

export default connect(mapStateToProps)(Conversation);
