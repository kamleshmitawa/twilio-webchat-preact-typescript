import CONVERSATION_TYPE from "../types";
import store from "../store";
import { AppActionTypes, actionsDataExpense } from "./ExpenseActions";

const { dispatch } = store;

export const chatScreenOpen = (data : actionsDataExpense ): AppActionTypes => {
  return dispatch({
    type: CONVERSATION_TYPE.CHAT_SCREEN_OPEN,
    payload: data
  });
};
export const chatScreenClose = (data :any): AppActionTypes => {
  return dispatch({
    type: CONVERSATION_TYPE.CHAT_SCREEN_CLOSE,
    payload: data
  });
};

export const conversationList = (data :any) => {
  return dispatch({
    type: CONVERSATION_TYPE.CONVERSATION_LIST,
    payload: data
  });
};

export const newMessage = (data : any) => {
  return dispatch({
    type: CONVERSATION_TYPE.NEW_MESSAGE,
    payload: data
  });
};
