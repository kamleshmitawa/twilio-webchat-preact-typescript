import { CONVERSATION_TYPES } from "../types/conversationTypes";

export interface actionsDataExpense {
  [key : string] : any
}

export interface conversationListExpense {
    type: typeof CONVERSATION_TYPES.CONVERSATION_LIST;
    payload: actionsDataExpense;
  }
  
  export interface chatScreenOpenExpense {
    type: typeof CONVERSATION_TYPES.CHAT_SCREEN_OPEN;
    payload: any;
  }
  
  export interface chatScreenCloseExpense {
    type: typeof CONVERSATION_TYPES.CHAT_SCREEN_CLOSE;
    payload: any;
  }
  export interface newMessageExpense {
    type: typeof CONVERSATION_TYPES.NEW_MESSAGE;
    payload: any;
  }
  
  
  
  export type ExpenseConversationActionTypes = conversationListExpense | chatScreenOpenExpense | chatScreenCloseExpense | newMessageExpense
  
  
  export type AppActionTypes = ExpenseConversationActionTypes
  
  // export type AppActionTypes = ExpenseConversationActionTypes| ExpenseChatActionTypes | more
  