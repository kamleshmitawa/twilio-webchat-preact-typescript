import uuidv1 from "uuid/v1";
import { getRequest } from "../api/api";

export const fetchChatToken = async () => {
  try {
    let res = await getRequest(`/web/token/${uuidv1()}`);
    return res.data.jwt;
  } catch (err) {
    console.log(err, "chat token error");
    return err
  }
};


export const fetchSyncToken = async () => {
  try{
    let res = await getRequest(`/web/sync-token/${uuidv1()}`);
    return res.data.jwt;
  }
  catch(err) {
    console.log(err, 'sync token error')
    return err
  }
}